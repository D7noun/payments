package org.D7noun.view;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import org.D7noun.model.Customer;
import org.D7noun.model.Payment;

@Stateful
@LocalBean
public class CustomerFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "payments-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public Customer findById(Long id) {
		try {
			return this.entityManager.find(Customer.class, id);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(" Customer findById");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Customer findByName(String name) {
		try {
			Query query = entityManager.createNamedQuery(Customer.findByName, Customer.class);
			query.setParameter("name", name);
			List<Customer> customers = query.getResultList();
			if (customers != null && !customers.isEmpty()) {
				return customers.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(" Customer findByName");
		}
		return null;
	}

	public List<Customer> getAll() {
		CriteriaQuery<Customer> criteria = this.entityManager.getCriteriaBuilder().createQuery(Customer.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Customer.class))).getResultList();
	}

	public void edit(Customer customer) {
		if (customer.getId() == null) {
			this.entityManager.persist(customer);
		} else {
			this.entityManager.merge(customer);
		}
	}

	public void remove(Customer customer) {
		try {
			Iterator<Payment> iterPayments = customer.getPayments().iterator();
			for (; iterPayments.hasNext();) {
				Payment nextInPayments = iterPayments.next();
				nextInPayments.setCustomer(customer);
				iterPayments.remove();
				this.entityManager.merge(nextInPayments);
			}
			this.entityManager.remove(customer);
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: Customer Remove");
		}
	}

	/**
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 */

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
