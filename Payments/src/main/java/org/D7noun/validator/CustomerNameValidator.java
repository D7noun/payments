package org.D7noun.validator;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.D7noun.model.Customer;
import org.D7noun.view.CustomerFacade;

@FacesValidator(value = "customerNameValidator")
public class CustomerNameValidator implements Serializable, Validator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CustomerFacade customerFacade;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Customer customer2 = customerFacade.findByName((String) value);
		if (customer2 != null) {
			Long id = (Long) component.getAttributes().get("param");
			Customer customer1 = null;
			if (id != null) {
				customer1 = customerFacade.findById(id);
			}
			if (customer1 == null) {
				FacesMessage msg = new FacesMessage("Duplicate Customer Name");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			} else {
				if (customer1 != customer2) {
					FacesMessage msg = new FacesMessage("Duplicate Customer Name");
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(msg);
				}
			}
		}
	}

}
